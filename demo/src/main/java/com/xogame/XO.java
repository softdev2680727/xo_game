package com.xogame;

import java.util.Scanner;

public class XO {
    static char[][] table;
    static char currentPlayer;
    static boolean gameOver = false;
    static Scanner kb = new Scanner(System.in);

    public XO() {
        table = new char[3][3];
        currentPlayer = 'X';
        createTable();
    }

    public static void createTable() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                table[row][col] = '-';
            }
        }
    }

    public static void testTableX() {
        table = new char[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                table[r][c] = 'X';
            }
        }
    }

    public static void testTableO() {
        table = new char[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                table[r][c] = 'O';
            }
        }
    }

    public void printTable() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print("| ");
            for (int col = 0; col < 3; col++) {
                System.out.print(table[row][col] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    public static boolean isTableFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkWin(char player) {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == player && table[row][1] == player && table[row][2] == player) {
                return true;
            }
        }
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == player && table[1][col] == player && table[2][col] == player) {
                return true;
            }
        }
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            return true;
        } else if (table[0][2] == player && table[1][1] == player && table[2][0] == player) {
            return true;
        }

        return false;
    }

    public void continueGame() {
        Scanner kb = new Scanner(System.in);
        boolean playAgain = true;
        System.out.print("Continue? (y/n): ");
        String choice = kb.next().toLowerCase();
        if (choice.equals("n")) {
            System.exit(0);
        } else if (choice.equals("y")) {
            startGame();
        } else {
            System.out.println("Please enter y or n");
        }
    }

    public void welcomeGame() {
        System.out.println("************************");
        System.out.println("*Welcome to XO Game !!!*");
        System.out.println("************************");
    }

    public void InputRowColumn() {
        while (!gameOver) {
            System.out.println("Player " + currentPlayer + "'s Turn");
            System.out.print("Enter row (1-3)and column (1-3): ");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;

            if (row < 0 || row >= 3 || col < 0 || col >= 3 || table[row][col] != ' ') {
                System.out.println("Invalid move. Try again.");
                continue;
            }
            table[row][col] = currentPlayer;
            System.out.println();
            printTable();
            if (checkWin(currentPlayer)) {
                System.out.println("Player " + currentPlayer + " wins!");
                gameOver = true;
            } else if (isTableFull()) {
                System.out.println("The game ends in a draw!");
                gameOver = true;
            }
            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
        }

    }

    public void startGame() {

        boolean playAgain = true;

        while (playAgain) {
            gameOver = false;
            welcomeGame();
            createTable();
            printTable();
            InputRowColumn();
            while (true) {
                continueGame();
            }
        }
        kb.close();
    }

    public static void main(String[] args) {
        XO game = new XO();
        game.startGame();
    }
}
