package com.xogame;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testCheckXDraw() {
        XO.testTableX();
        boolean result = XO.isTableFull();
        assertTrue(result);
    }

    @Test
    public void testCheckYDraw() {
        XO.testTableO();
        boolean result = XO.isTableFull();
        assertTrue(result);
    }

    @Test
    public void testCheckXWin() {
        XO.testTableX();
        boolean result = XO.checkWin('X');
        assertTrue(result);
    }

    @Test
    public void testCheckOWin() {
        XO.testTableO();
        boolean result = XO.checkWin('O');
        assertTrue(result);
    }

    @Test
    public void testCheckXLose() {
        XO.testTableO();
        boolean result = XO.checkWin('X');
        assertEquals(false,result);
    }

    @Test
    public void testCheckOLose() {
        XO.testTableX();
        boolean result = XO.checkWin('O');
        assertEquals(false,result);
    }

}
